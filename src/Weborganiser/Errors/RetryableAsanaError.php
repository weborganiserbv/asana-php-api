<?php

namespace Weborganiser\Errors;

use Weborganiser\Errors\AsanaError;

class RetryableAsanaError extends AsanaError
{
}
